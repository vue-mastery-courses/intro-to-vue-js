Vue.component('product-review', {
    props: {

    },
    template: 
        `
        <p>dfadf</p>`
    
})

Vue.component('product-details', {
    props: {
        details: {
            type: Array,
            required: true,
        },
    },
    template:
        `
        <ul>
            <li v-for="detail in details">
                {{ detail }}
            </li>
        </ul>
        `
})
Vue.component('product', {
    props: {
        premium: {
            type: Boolean,
            required: true,
        }
    },
    template: `
    <div class="product">
            <div class="product-image">
                <img :src="image" alt="">
            </div>
            <div class="product-info">
                <h1>{{ title }}</h1>
                <p>User is premium: {{premium}}</p>
                <p v-if="inventory > 10">In Stock</p>
                
                <p v-else-if="inventory <= 10 && inventory > 0">Almost sold out. Hurry!</p>
                <p v-else>Out of stock</p>
              
                <p v-show="onSale" style="color: red;">{{sale}}</p>
                <product-details :details="details"></product-details>


                <div v-for="(variant, index) in variants" 
                    :key="variant.variantId" 
                    class="color-box"
                    :style="{ backgroundColor: variant.variantColor}"
                    @mouseover="updateProduct(index)">
                    <p ></p>
                </div>
                    
                <div>
                    <p v-if="!inStock">Out of stock</p>
                </div>

                <button v-on:click="addToCart" 
                    :disabled="!inStock"
                    :class="{ disabledButton: !inStock }"
                    >Add to cart</button>
                <button v-on:click="removeFromCart">Remove from cart</button>

                <p>Shipping: {{shipping}}</p>

            </div>
        </div>
    `,
    data() {
        return {
            brand: 'Vue Mastery',
            product: 'Socks',
            selectedVariant: 0,
            inventory: 5,
            onSale: false,
            details: ["80% cotton", "20% polyester", "gender-neutral"],
            variants: [
                {
                    variantId: 1231,
                    variantColor: "green",
                    variantImage: "assets/vmSocks-green-onWhite.jpg",
                    variantQuantity: 0
                },
                {
                    variantId: 1233,
                    variantColor: "blue",
                    variantImage: "assets/vmSocks-blue-onWhite.jpg",
                    variantQuantity: 99
                },
            ],
            sizes: ["small", "medium", "large"],
            
        }
    },
    methods: {

        removeFromCart: function () {
            this.$emit('remove-from-cart', this.variants[this.selectedVariant].variantId)
            
        },
        updateProduct: function (index) {
            this.selectedVariant = index
        },
        addToCart: function () {
            
            this.$emit('add-to-cart', this.variants[this.selectedVariant].variantId)
        },
    },
    computed: {
        title() {
            return (this.brand + ' ' + this.product)
        },
        image() {
            return this.variants[this.selectedVariant].variantImage
        },
        inStock() {
            return this.variants[this.selectedVariant].variantQuantity
        },
        sale() {
            if (this.onSale) {
                return this.brand + ' ' + this.product + ' On Sale'
            }
        },
        shipping() {
            if (this.premium) {
                return "Free"
            } else {
                return 2.99
            }
        }
    }
})

var app = new Vue({
    el: '#app',
    data: {
        premium: true,
        cart: [],
    },
    methods: {
        updateCart (id) {
            this.cart.push(id)
        },
        removeItem (id) {
            console.log(id)
            const index = this.cart.indexOf(id)
            console.log(index)
            if (index > -1) {
                this.cart.splice(index, 1);
            }

        }
    },
    computed: {

    }
})